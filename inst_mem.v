module inst_mem (
    input[31:0] i,
    output[31:0] out
);

    reg[31:0] mem[0:1023];

    initial begin
        $readmemb ("inst.txt", mem);
    end

    assign out = mem[i>>2];

endmodule