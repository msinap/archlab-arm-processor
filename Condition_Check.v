module Condition_Check
(
    input [3:0] cond,
    input [3:0] SR,
    output reg Is_True
);
    
    always @(*) begin
        Is_True = 1'b0;
        case (cond)
            4'b0000: Is_True = SR[2];
            4'b0001: Is_True = !SR[2];
            4'b0010: Is_True = SR[1];
            4'b0011: Is_True = !SR[1];
            4'b0100: Is_True = SR[3];
            4'b0101: Is_True = !SR[3];
            4'b0110: Is_True = SR[0];
            4'b0111: Is_True = !SR[0];
            4'b1000: Is_True = SR[1] ? !SR[2] : 1'b0;
            4'b1001: Is_True = SR[2] ? !SR[1] : 1'b0;
            4'b1010: Is_True = SR[3] == SR[0] ? 1'b1 : 1'b0;
            4'b1011: Is_True = SR[3] != SR[0] ? 1'b1 : 1'b0;
            4'b1100: Is_True = SR[2] ? 1'b0 : (SR[3] == SR[0] ? 1'b1 : 1'b0);
            4'b1101: Is_True = SR[2] ? (SR[3] != SR[0] ? 1'b1 : 1'b0) : 1'b0;
            4'b1110: Is_True = 1'b1;
            default: Is_True = 1'b0;
        endcase
        
    end

endmodule