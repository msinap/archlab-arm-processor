module val2tb();

    reg[11:0] Shift_operand;
    reg[31:0] Val_Rm;
    reg imm, mem;

    wire[31:0] Val2;

    Val2_Generator val2 (
        .Shift_operand(Shift_operand),
        .Val_Rm(Val_Rm),
        .imm(imm),
        .mem(mem),
        
        .Val2(Val2)
    );

    initial begin
        Shift_operand = 12'b0010_01100011;
        imm = 1;
        mem = 0;
        #10;
        Val_Rm = 32'b11100011101010000100110000001111;
        imm = 0;
        Shift_operand = 12'b00001_00_00000;
        #10;
        Shift_operand = 12'b00001_01_00000;
        #10;
        Shift_operand = 12'b00001_10_00000;
        #10;
        Shift_operand = 12'b00001_11_00000;
        #10;
        Shift_operand = 12'b10001_11_00000;
        mem = 1;
        #10;
        Shift_operand = 12'b000000010100;
        mem = 0;
        imm = 1;
        #10;
    end

endmodule