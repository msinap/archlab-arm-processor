module alutb();

    reg[31:0] val1, val2;
    reg[3:0] command;
    reg Cin;
    wire[31:0] out;
    wire Cout, Z, N, V;

    ALU alu(
        .val1(val1),
        .val2(val2),
        .command(command),
        .Cin(Cin),
        .out(out),
        .Cout(Cout),
        .Z(Z),
        .N(N),
        .V(V)
    );

    initial begin
        Cin = 1;
        val1 = 5;
        val2 = 8;
        command = 4'b0001;
        #10;
        command = 4'b1001;
        #10;
        command = 4'b0010;
        #10;
        command = 4'b0011;
        #10;
        command = 4'b0100;
        #10;
        command = 4'b0101;
        #10;
        command = 4'b0110;
        #10;
        command = 4'b0111;
        #10;
        command = 4'b1000;
        #10;
    end


endmodule